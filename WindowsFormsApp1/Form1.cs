﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private double a = 0;
        private double b = 0;
        private double c = 0;
        private double d = 0;
        public double gestosczapisu = 20;

        

        private PointF[] p = new PointF[2000];

        public Form1()
        {
            InitializeComponent();
            
        }


        

        private void thpower()
        {
            double skalowanie = 1 / gestosczapisu;
            for (int x = -1000; x < 1000; x++)
            {
                double res = a * Math.Pow(x, 3);
                res = -(res);
                res = res * skalowanie * skalowanie;

                p[x + 1000] = new PointF(x, (float)res);
            }
        }

        private void square()
        {
            double skalowanie = 1/gestosczapisu;
            for (int x = -1000; x < 1000; x++)
            { 
                double res = b * Math.Pow(x, 2);
                res = -(res);
                res = res * skalowanie;

                p[x + 1000] = new PointF(x, (float)res);

            }
        }

        private void sinus()
        {
            double skalowanie = 1 / gestosczapisu;
            for (int x = -1000; x < 1000; x++)
            {
                double res = c * Math.Sin(x*skalowanie);
                res = -(res);
                //res = res * skalowanie;

                p[x + 1000] = new PointF(x, (float)res*(float)gestosczapisu);
            }
        }

        private void linearf()
        {
            double skalowanie = 1 / gestosczapisu;
            for (int x = -1000; x < 1000; x++)
            {
                double res = d * x;
                res = -(res);
               

                p[x + 1000] = new PointF(x, (float)res);
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
            Pen pen = new Pen(Color.LightGray, 1);
            Pen penpen = new Pen(Color.Red);
            Pen penpenpen = new Pen(Color.Black, 1);

            Font richtigfajnyfont = new Font("Arial", 7);
            SolidBrush richtigfajnobrush = new SolidBrush(Color.Black);

            int formWidth = panel1.Width;
            int formHeight = panel1.Height;

            double I_YYYAxVal = 0;
            double I_XXXAxVal = 0;

            int y = 0;
            int x = 0;
            int sterx = 0;
            int stery = 0;

            Point middlemiddle = new Point(formWidth / 2, formHeight / 2);
            Point leftmiddle = new Point(0, formHeight / 2);
            Point rightmiddle = new Point(formWidth, formHeight / 2);
            Point topmiddle = new Point(formWidth / 2, 0);
            Point bottommiddle = new Point(formWidth / 2, formHeight);

            Point polygon1a = new Point(formWidth - 10, (formHeight / 2) - 10);
            Point polygon1b = new Point(formWidth - 10, (formHeight / 2) + 10);
            Point polygon2a = new Point((formWidth / 2) - 10, 10);
            Point polygon2b = new Point((formWidth / 2) + 10, 10);
            Point[] polygon1 = { polygon1a, polygon1b, rightmiddle };
            Point[] polygon2 = { polygon2a, polygon2b, topmiddle };

            a = double.Parse(textBox1.Text.Replace(',', '.'), CultureInfo.InvariantCulture);
            b = double.Parse(textBox2.Text.Replace(',', '.'), CultureInfo.InvariantCulture);
            c = double.Parse(textBox3.Text.Replace(',', '.'), CultureInfo.InvariantCulture);
            d = double.Parse(textBox4.Text.Replace(',', '.'), CultureInfo.InvariantCulture);

            for (y = formHeight/2; y <= formHeight; y += (int)gestosczapisu)
            {
                e.Graphics.DrawLine(pen, 0, y, formWidth, y);
                e.Graphics.DrawLine(penpenpen, (formWidth/2)-2, y, (formWidth/2)+2, y); 

                if (stery % 2 == 0)
                {
                    String S_YYYAxVal = Convert.ToString(I_YYYAxVal);
                    e.Graphics.DrawString(S_YYYAxVal, richtigfajnyfont, richtigfajnobrush, (formWidth/2), y);
                    
                }
                I_YYYAxVal+=1;
                stery += 1;
            }
            I_YYYAxVal = 0;
            stery = 0;
            for (y = formHeight/2; y >= 0; y -= (int)gestosczapisu)
            {
                e.Graphics.DrawLine(pen, 0, y, formWidth, y);
                e.Graphics.DrawLine(penpenpen, (formWidth / 2) - 2, y, (formWidth / 2) + 2, y);

                if (stery % 2 == 0)
                {
                    String S_YYYAxVal = Convert.ToString(I_YYYAxVal);
                    e.Graphics.DrawString(S_YYYAxVal, richtigfajnyfont, richtigfajnobrush, (formWidth / 2), y);

                }
                I_YYYAxVal -= 1;
                stery += 1;
            }

            for (x = formWidth/2; x <= formWidth; x += (int)gestosczapisu)
            {
                e.Graphics.DrawLine(pen, x, 0, x, formHeight);  
                e.Graphics.DrawLine(penpenpen, x, (formHeight / 2)-2, x, (formHeight / 2) + 2);

                if (sterx % 2 == 0)
                {
                    String S_XXXAxVal = Convert.ToString(I_XXXAxVal);
                    e.Graphics.DrawString(S_XXXAxVal, richtigfajnyfont, richtigfajnobrush, x, (formHeight/2));
                    
                }
                I_XXXAxVal+=1;
                sterx += 1;
            }
            sterx = 0;
            I_XXXAxVal = 0;
            for (x = formWidth/2; x >= 0; x -= (int)gestosczapisu)
            {
                e.Graphics.DrawLine(pen, x, 0, x, formHeight);
                e.Graphics.DrawLine(penpenpen, x, (formHeight / 2) - 2, x, (formHeight / 2) + 2);

                if (sterx % 2 == 0)
                {
                    String S_XXXAxVal = Convert.ToString(I_XXXAxVal);
                    e.Graphics.DrawString(S_XXXAxVal, richtigfajnyfont, richtigfajnobrush, x, (formHeight / 2));

                }
                I_XXXAxVal -= 1;
                sterx += 1;
            }

            e.Graphics.DrawLine(penpenpen, middlemiddle, leftmiddle); //drawing the black x-axis
            e.Graphics.DrawLine(penpenpen, middlemiddle, rightmiddle);
            e.Graphics.DrawLine(penpenpen, middlemiddle, topmiddle); //drawing the black y-axis
            e.Graphics.DrawLine(penpenpen, middlemiddle, bottommiddle);

            e.Graphics.FillPolygon(richtigfajnobrush, polygon1);
            e.Graphics.FillPolygon(richtigfajnobrush, polygon2);
            
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            e.Graphics.TranslateTransform(formWidth/2, formHeight/2);
            e.Graphics.ScaleTransform(1f, 1f);
            e.Graphics.DrawLines(penpen, p);
            
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (Bitmap graphicSurface = new Bitmap(panel1.Width, panel1.Height))
            {
                panel1.DrawToBitmap(graphicSurface, new Rectangle(0, 0, panel1.Width, panel1.Height));
                graphicSurface.Save("obrazek.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            for (int h = 58; h <= 127; h++)
            {
                if (e.KeyChar == h)            
                {
                    e.Handled = true;
                }
            }
            int wartownik;
            for (int k = 32; k <= 47; k++)
            {
                if (e.KeyChar == k)             
                {
                    if (k != 44 && k != 45 && k != 46)
                    {
                        e.Handled = true;
                    /*    if (k == 44 || k == 45)
                        {
                            wartownik = 1;
                        }
                        if (wartownik == 1)
                        {
                            e.Handled = false;
                        }
                        */
                    }
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            gestosczapisu -= 2;
            Refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            gestosczapisu += 2;
            Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            thpower();
            Refresh();
            Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            square();
            Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            sinus();
            Refresh();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            linearf();
            Refresh();
        }
    }
}
